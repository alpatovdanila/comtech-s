

/**
 * Event bus provider. Thats just a fancy name for Pub-Sub pattern realisation;
 */
class Bus{

    constructor(){
        this.subscribers = [];
    }

    /**
     * Subscribe element to event
     * @param {HTMLElement} element
     * @param {string} eventName
     */
    subscribe(topic, handler){
        if(this.subscribers[topic] === undefined) this.subscribers[topic] = [];
        let index = this.subscribers[topic].push(handler) - 1;
        return {
            remove: ()=>{
                delete this.subscribers[topic][index]
            }
        }

    }

    /**
     * Broadcats event to all subscribers
     * @param event
     */
    publish({topic, payload}){
        if(this.subscribers[topic] === undefined){
            //console.info('POINTLESS BROADCAST WARNING: There is no subscribers found for '+topic);
        }else{
            this.subscribers[topic].forEach( handler => {
                handler( payload );
            })

        }

        AppConfig.debug && console.dir({topic, payload});
    }



    /**
     * Get all subscribed listeners for event name
     * @param eventType
     * @return {*}
     */
    getSubscribers( topic ){
        return this.subscribers[topic];
    }
}

// Exporting as singletone;

export const EventBus = new Bus();