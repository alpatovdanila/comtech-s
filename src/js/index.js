import '../scss/index.scss';
/**
 * Returns an array with all DOM elements affected by an event.
 * The function serves as a polyfill for
 * [`Event.composedPath()`](https://dom.spec.whatwg.org/#dom-event-composedpath).
 *
 * @category Event
 * @param {Event} evt The triggered event.
 * @return {Array.<HTMLElement>} The DOM elements affected by the event.
 *
 * @example
 * let domChild = document.createElement("div"),
 *  domParent = document.createElement("div"),
 *  domGrandparent = document.createElement("div"),
 *  body = document.body,
 *  html = document.querySelector("html");
 *
 * domParent.appendChild(domChild);
 * domGrandparent.appendChild(domParent);
 * body.appendChild(domGrandparent);
 *
 * domChild.addEventListener("click", dealWithClick);
 * const dealWithClick = evt => getEventPath(evt);
 *
 * // when domChild is clicked:
 * // => [domChild, domParent, domGrandparent, body, html, document, window]
 *
 *
 * https://gist.github.com/leofavre/d029cdda0338d878889ba73c88319295
 */
export function eventPath(evt) {
    var path = (evt.composedPath && evt.composedPath()) || evt.path,
        target = evt.target;

    if (path != null) {
        // Safari doesn't include Window, and it should.
        path = (path.indexOf(window) < 0) ? path.concat([window]) : path;
        return path;
    }

    if (target === window) {
        return [window];
    }

    function getParents(node, memo) {
        memo = memo || [];
        var parentNode = node.parentNode;

        if (!parentNode) {
            return memo;
        }
        else {
            return getParents(parentNode, memo.concat([parentNode]));
        }
    }

    return [target]
        .concat(getParents(target))
        .concat([window]);
}
const offcanvas = document.querySelector('.offcanvas');

/**
 * Listens for click outside of an element
 * @param {HTMLElement} element
 * @param {function} callback
 * @param {boolean} immediateRemove should listener be removed after first callback call. True by default.
 */
export function listenClickOutside(element, callback, immediateRemove = true) {
    document.addEventListener('click', function handler(event) {
        if (eventPath(event).indexOf(element) === -1) {
            callback(event);
            if (immediateRemove) document.removeEventListener('click', handler);
        }
    })


}

class offCanvas{

    constructor(el){
        this.el = el;
        this.opened = el.classList.contains('show');

    }

    hide(){
        this.el.classList.remove('show');
        this.opened =false;

    }

    show(){
        this.el.classList.add('show');
        this.opened =true;
        listenClickOutside(this.el, (e)=> {
            if (!(eventPath(e).includes(offcanvas))) this.hide() // Извините, я тут задачи бизнеса решаю. Потом отрефачу. Когда-нибудь.


        });
    }

    toggle(){
        this.opened ? this.hide() : this.show();
    }
}

const off = new offCanvas(offcanvas);

document.querySelectorAll('[data-bind="ToggleOffCanvas"]').forEach( toggler => {
    toggler.addEventListener('click', ()=>{
        window.requestAnimationFrame(()=>{
            off.toggle();
        })
    });
})



const Tree = element => {
    let id = element.id;
    let state = {};

    const openNode = (node, index) => {
        node.classList.add('open');
        state[index] = true;
    };

    const closeNode = (node,index) => {
        node.classList.remove('open');
        state[index] = false;
    };

    let nodes = Array.from(element.getElementsByTagName('li')).filter( el => !!el.getElementsByTagName('ul').length);

    console.log(nodes);
    nodes.forEach( (node, index) => {
        node.classList.add('node');
        if(!state[index]) state[index] = false; else openNode(node, index);

        node.addEventListener('click', event =>{
            event.stopPropagation();
            let opened = state[index];
            opened ? closeNode(node, index) : openNode(node,index);
            storeState(id, state);
        })
    })


};

const restoreState = key => {
    return JSON.parse(localStorage.getItem(key)) || {};
};

const storeState = (key, state) => {
    localStorage.setItem(key, JSON.stringify(state));
};

document.querySelectorAll('[data-component="Tree"]').forEach(Tree);




