<?php require_once  'templates/documentHeader.php'?>
    <div class="container">
        <div class="index-carland">
            <div class="row">
                <div class="col-12">
                    <div class="toolbar">
                        <div class="row no-gutters">
                            <div class="col-6">
                                <button class="btn btn_brand-yellow btn_hamburger" data-bind="ToggleOffCanvas"><img src="src/gfx/icon-hamburger.png"
                                                                                        alt="" class="icon icon_first">
                                    Показать каталог
                                </button>
                            </div>
                            <div class="col-6 text-right">
                                <button class="btn btn_brand-yellow btn_cart"><img src="src/gfx/icon-basket.png" alt=""
                                                                                   class="icon icon_first"> Корзина
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="index-carland__headline text-center">Проблемы с техникой?<br>
                        Хорошо, что вы попали к нам!
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-3 text-right">

                    <div class="index-carland__txtblock index-carland__txtblock_left">
                        <h2>РЕМОНТ
                            ОБОРУДОВАНИЯ</h2>
                        <p class="index-carland__paragraph">Ремонтируем, устанавливаем, модернизируем почти любое
                            специальное оборудование</p>
                    </div>
                    <div class="index-carland__txtblock index-carland__txtblock_left">
                        <h2>РЕМОНТ ШАССИ</h2>
                        <p class="index-carland__paragraph">Ремонтируем и делаем ТО
                            узлов и агрегатов</p>
                    </div>

                </div>
                <div class="col-6 text-center">
                    <img class="index-carland__car" src="src/gfx/landing-car.png">
                </div>
                <div class="col-3">


                    <div class="index-carland__txtblock ">
                        <h2>ЗАПЧАСТИ</h2>
                        <p class="index-carland__paragraph">Используем сами и продаём запчасти и расходные материалы для
                            шасси и оборудования</p>
                    </div>


                    <div class="index-carland__txtblock">
                        <h2>ВЫЕЗДНАЯ БРИГАДА</h2>
                        <p class="index-carland__paragraph">Приезжаем по вызову на диагностику, ремонт или
                            обслуживание</p>
                    </div>

                </div>


            </div>
            <div class="col-12">
                <div class="index-carland__actions">
                    <button class="btn btn_brand-yellow btn_block">ИЗБАВИТЬСЯ ОТ ПРОБЛЕМ</button>
                </div>
            </div>

        </div>
        <div class="index-shortland">
            <span class="index-shortland__headline">
                ЕСЛИ ЧУТЬ КОНКРЕТНЕЕ
            </span>
            <ui class="index-list">
                <li class="index-list__item"><b>Ремонт гидравлики</b> на экскаваторах, погрузчиках и коммунальной
                    технике
                </li>
                <li class="index-list__item"><b>Ремонт тракторов МТЗ-82</b>: КПП, замена сцепления, передний мост</li>
                <li class="index-list__item">Выездной <b>ремонт бульдозеров</b></li>
                <li class="index-list__item"><b>Ремонт каналопромывочных машин</b> и насосов</li>
            </ui>
            <p class="index-shortland__paragraph">
                И&nbsp;другие работы по&nbsp;ремонту, модернизации, обслуживанию спецтехники на&nbsp;любых шасси и&nbsp;в&nbsp;любых
                модификациях.<br>
                В&nbsp;нашем ангаре или с&nbsp;выездом бригады на&nbsp;место стоянки вашей техники.
            </p>
            <div class="index-shortland__actions">
                <div class="stack align-items-center">
                    <div class="stack__item">
                        <button class="btn btn_brand-yellow">Вернуть технику в строй</button>
                    </div>
                    <div class="stack__item">ИЛИ</div>
                    <div class="stack__item">
                        <button class="btn btn_brand-yellow_outline">Ещё походить-посмотреть</button>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php require_once  'templates/documentFooter.php'?>



<script src="dist/main.bundle.js"></script>
</body>
</html>