<div class="container">
    <div class="footer">
        <div class="row">
            <div class="col-2">
                <h3 class="footer__heading">Информация</h3>
                <ul class="footer__nav">
                    <li><a href="" class="link link_underline">О компании</a></li>
                    <li><a href="" class="link link_underline">Услуги</a></li>
                    <li><a href="" class="link link_underline">Контакты</a></li>
                </ul>
            </div>
            <div class="col-2">
                <h3 class="footer__heading">Каталог</h3>
                <ul class="footer__nav">
                    <li><a href="" class="link link_underline">Запчасти</a></li>
                    <li><a href="" class="link link_underline">Крупные узлы и агрегаты</a></li>
                    <li><a href="" class="link link_underline">Техника в наличии</a></li>
                </ul>
            </div>
            <div class="col-4">
                <h4>+7 922 345-21-11</h4>
                <p>Общество с ограниченной ответственностью «Комтех-сервис»
                    140123, Московская область, Раменский район ,
                    Верхнее Мячково с., Аэропорт</p>
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="col-12 text-right">
                        <div class="footer__copyright">
                            © 2018 «<a href="/" class="link link_underline">КОМТЕХ-СЕРВИС</a>»
                        </div>
                    </div>
                </div>

                <div class="footer__logos">
                    <div class="row justify-content-end">
                        <div class="col-auto"><img src="src/gfx/cn-digital-logo.png" alt=""></div>
                        <div class="col-auto"><img src="src/gfx/spikecms-logo.png" alt=""></div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>