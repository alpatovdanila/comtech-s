<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Comtech-s</title>

</head>
<body>
<link rel="stylesheet" href="dist/style.css">
<div class="offcanvas">
    <div class="offcanvas__container">
        <ul class="sidemenu">
            <li class="sidemenu__section">УСЛУГИ</li>
            <ul class="sidemenu__node">
                <li><a href="" class="link link_underline link_white">Ремонт</a></li>
                <li><a href="" class="link link_underline link_white">Техническое обслуживание</a></li>
                <li><a href="" class="link link_underline link_white">Установка оборудования</a></li>
                <li><a href="" class="link link_underline link_white">Выездная бригада</a></li>
            </ul>
            <li class="sidemenu__section">Запчасти</li>
            <ul class="sidemenu__node" data-component="Tree">


                <li>Коммунальное оборудование
                    <ul>


                        <li><a href="" class="link link_underline link_white"> Коммунальное оборудование</a></li>
                        <li><a href="" class="link link_underline link_white">Щетки дисковые, секторы</a></li>
                        <li><a href="" class="link link_underline link_white">Гидравлика</a></li>
                        <li><a href="" class="link link_underline link_white"> Рукава, техпластина, цепи</a></li>

                    </ul>
                </li>
                <li><a href="" class="link link_underline link_white">Щетки дисковые, секторы</a></li>
                <li><a href="" class="link link_underline link_white">Гидравлика</a></li>
                <li><a href="" class="link link_underline link_white"> Рукава, техпластина, цепи</a></li>

            </ul>
            <li class="sidemenu__section">УСЛУГИ</li>
            <ul class="sidemenu__node">
                <li><a href="" class="link link_underline link_white">Ремонт</a></li>
                <li><a href="" class="link link_underline link_white">Техническое обслуживание</a></li>
                <li><a href="" class="link link_underline link_white">Установка оборудования</a></li>
                <li><a href="" class="link link_underline link_white">Выездная бригада</a></li>
            </ul>
        </ul>
    </div>

    <div class="offcanvas__cta">
        <button class="btn btn_brand-yellow">ЗАПРОСИТЬ КОНСУЛЬТАЦИЮ</button>
    </div>
</div>
<div id="canvas">
    <div class="container">
        <div class="page-header">
            <div class="row align-items-center">
                <div class="col-2 text-left">
                    <img src="src/gfx/comtech-logo.svg" class="logo">
                </div>

                <div class="col-7">
                    <ul class="mainmenu">
                        <li class="mainmenu__item"><a href="" class="mainmenu__link mainmenu__link_active">О
                                компании</a>
                        </li>
                        <li class="mainmenu__item"><a href="" class="mainmenu__link">Обслуживание</a></li>
                        <li class="mainmenu__item"><a href="" class="mainmenu__link">Техника с хранения</a></li>
                        <li class="mainmenu__item"><a href="" class="mainmenu__link">Запчасти</a></li>
                        <li class="mainmenu__item"><a href="" class="mainmenu__link">Статьи</a></li>
                        <li class="mainmenu__item"><a href="" class="mainmenu__link">Контакты</a></li>
                    </ul>
                </div>
                <div class="col-3 text-right">
                    <div class="stack">
                        <div class="stack__item"><a href="" class="link link_underline">Наш цех на карте</a></div>
                        <div class="stack__item"><a href="tel:74959220576" class="phone-link ">+7(495) 922-05-76</a>
                        </div>
                    </div>

                </div>


            </div>
        </div>

    </div>