<?php require_once 'templates/documentHeader.php' ?>
<div class="container">
    <div class="superbanner">
        <div class="row align-items-center">
            <div class="col-3">
                <div class="superbanner__title">
                    21 ВЕК НА ДВОРЕ!
                </div>
            </div>
            <div class="col-4 text-right">
                <div class="superbanner__text">ПРОЩЕ НАПИСАТЬ СООБЩЕНИЕ, ЧЕМ РАЗБИРАТЬСЯ САМОМУ</div>
            </div>
            <div class="col-1 text-canter">
                <img src="src/gfx/arrow.png" alt="" class="superbanner__row">
            </div>
            <div class="col-4">
                <button class="btn btn_brand-yellow btn_block">Написать специалисту по запчастям</button>
            </div>
        </div>
        <div class="superbanner-headline"></div>

    </div>

</div>
<div class="container">
    <div class="content-layout">
        <div class="toolbar">
            <div class="row no-gutters">
                <div class="col-6">
                    <button class="btn btn_brand-yellow btn_hamburger" data-bind="ToggleOffCanvas"><img
                                src="src/gfx/icon-hamburger.png"
                                alt="" class="icon icon_first">
                        Показать каталог
                    </button>
                </div>
                <div class="col-6 text-right">
                    <button class="btn btn_brand-yellow btn_cart"><img src="src/gfx/icon-basket.png" alt=""
                                                                       class="icon icon_first"> Корзина
                    </button>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="stack stack_compact">
                <div class="stack__item">ТЕХНИКА С ХРАНЕНИЯ</div>
                <div class="stack__item">></div>

            </div>
        </div>
        <h1 class="page-title">
            МИНИ-ПОГРУЗЧИКИ
        </h1>
        <div class="catalog-matrix">
            <div class="product-card">
                <div class="row no-gutters">
                    <div class="col-5"><img class="product-card__image"
                                            src="http://stroy-plys.ru/uploads/posts/2013-12/1387990640_kitayskie-mini-pogruzchiki-liugong.jpg">
                    </div>
                    <div class="col-7">
                        <div class="product-card__info">
                            <div class="row">
                                <div class="col-8"><h2>JCB-1233</h2>
                                    <ul class="product-card__specs">
                                        <li>Ковш: <b>2м3</b></li>
                                        <li>Грузоподъёмность: <b>1200кг</b></li>
                                        <li>Год выпуска: <b>2012</b></li>
                                        <li></li>
                                    </ul>
                                    <p class="product-card__description">
                                        Ковшовый мини-погрузчик JCB. Наработка двигателя - 100 моточасов, пройдено ТО.
                                        Погрузчик работал на площадке крупной компании, выкуплен, собственник — наша
                                        компания
                                    </p>
                                    <div class="product-card__cta">
                                        <button class="btn btn_brand-yellow">ЗАПРОСИТЬ ПОДРОБНОСТИ</button>
                                    </div>
                                </div>
                                <div class="col-4 text-right">
                                    <span
                                            class="product-card__price">1 700 000 <s>Р</s></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="product-card">
                <div class="row no-gutters">
                    <div class="col-5"><img class="product-card__image"
                                            src="http://stroy-plys.ru/uploads/posts/2013-12/1387990640_kitayskie-mini-pogruzchiki-liugong.jpg">
                    </div>
                    <div class="col-7">
                        <div class="product-card__info">
                            <div class="row">
                                <div class="col-8"><h2>JCB-1233</h2>
                                    <ul class="product-card__specs">
                                        <li>Ковш: <b>2м3</b></li>
                                        <li>Грузоподъёмность: <b>1200кг</b></li>
                                        <li>Год выпуска: <b>2012</b></li>
                                        <li></li>
                                    </ul>
                                    <p class="product-card__description">
                                        Ковшовый мини-погрузчик JCB. Наработка двигателя - 100 моточасов, пройдено ТО.
                                        Погрузчик работал на площадке крупной компании, выкуплен, собственник — наша
                                        компания
                                    </p>
                                    <div class="product-card__cta">
                                        <button class="btn btn_brand-yellow">ЗАПРОСИТЬ ПОДРОБНОСТИ</button>
                                    </div>
                                </div>
                                <div class="col-4 text-right">
                                    <span
                                            class="product-card__price">1 700 000 <s>Р</s></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="product-card">
                <div class="row no-gutters">
                    <div class="col-5"><img class="product-card__image"
                                            src="http://stroy-plys.ru/uploads/posts/2013-12/1387990640_kitayskie-mini-pogruzchiki-liugong.jpg">
                    </div>
                    <div class="col-7">
                        <div class="product-card__info">
                            <div class="row">
                                <div class="col-8"><h2>JCB-1233</h2>
                                    <ul class="product-card__specs">
                                        <li>Ковш: <b>2м3</b></li>
                                        <li>Грузоподъёмность: <b>1200кг</b></li>
                                        <li>Год выпуска: <b>2012</b></li>
                                        <li></li>
                                    </ul>
                                    <p class="product-card__description">
                                        Ковшовый мини-погрузчик JCB. Наработка двигателя - 100 моточасов, пройдено ТО.
                                        Погрузчик работал на площадке крупной компании, выкуплен, собственник — наша
                                        компания
                                    </p>
                                    <div class="product-card__cta">
                                        <button class="btn btn_brand-yellow">ЗАПРОСИТЬ ПОДРОБНОСТИ</button>
                                    </div>
                                </div>
                                <div class="col-4 text-right">
                                    <span
                                            class="product-card__price">1 700 000 <s>Р</s></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="product-card">
                <div class="row no-gutters">
                    <div class="col-5"><img class="product-card__image"
                                            src="http://stroy-plys.ru/uploads/posts/2013-12/1387990640_kitayskie-mini-pogruzchiki-liugong.jpg">
                    </div>
                    <div class="col-7">
                        <div class="product-card__info">
                            <div class="row">
                                <div class="col-8"><h2>JCB-1233</h2>
                                    <ul class="product-card__specs">
                                        <li>Ковш: <b>2м3</b></li>
                                        <li>Грузоподъёмность: <b>1200кг</b></li>
                                        <li>Год выпуска: <b>2012</b></li>
                                        <li></li>
                                    </ul>
                                    <p class="product-card__description">
                                        Ковшовый мини-погрузчик JCB. Наработка двигателя - 100 моточасов, пройдено ТО.
                                        Погрузчик работал на площадке крупной компании, выкуплен, собственник — наша
                                        компания
                                    </p>
                                    <div class="product-card__cta">
                                        <button class="btn btn_brand-yellow">ЗАПРОСИТЬ ПОДРОБНОСТИ</button>
                                    </div>
                                </div>
                                <div class="col-4 text-right">
                                    <span
                                            class="product-card__price">1 700 000 <s>Р</s></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="product-card">
                <div class="row no-gutters">
                    <div class="col-5"><img class="product-card__image"
                                            src="http://stroy-plys.ru/uploads/posts/2013-12/1387990640_kitayskie-mini-pogruzchiki-liugong.jpg">
                    </div>
                    <div class="col-7">
                        <div class="product-card__info">
                            <div class="row">
                                <div class="col-8"><h2>JCB-1233</h2>
                                    <ul class="product-card__specs">
                                        <li>Ковш: <b>2м3</b></li>
                                        <li>Грузоподъёмность: <b>1200кг</b></li>
                                        <li>Год выпуска: <b>2012</b></li>
                                        <li></li>
                                    </ul>
                                    <p class="product-card__description">
                                        Ковшовый мини-погрузчик JCB. Наработка двигателя - 100 моточасов, пройдено ТО.
                                        Погрузчик работал на площадке крупной компании, выкуплен, собственник — наша
                                        компания
                                    </p>
                                    <div class="product-card__cta">
                                        <button class="btn btn_brand-yellow">ЗАПРОСИТЬ ПОДРОБНОСТИ</button>
                                    </div>
                                </div>
                                <div class="col-4 text-right">
                                    <span
                                            class="product-card__price">1 700 000 <s>Р</s></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
            <div class="product-card">
                <div class="row no-gutters">
                    <div class="col-5"><img class="product-card__image"
                                            src="http://stroy-plys.ru/uploads/posts/2013-12/1387990640_kitayskie-mini-pogruzchiki-liugong.jpg">
                    </div>
                    <div class="col-7">
                        <div class="product-card__info">
                            <div class="row">
                                <div class="col-8"><h2>JCB-1233</h2>
                                    <ul class="product-card__specs">
                                        <li>Ковш: <b>2м3</b></li>
                                        <li>Грузоподъёмность: <b>1200кг</b></li>
                                        <li>Год выпуска: <b>2012</b></li>
                                        <li></li>
                                    </ul>
                                    <p class="product-card__description">
                                        Ковшовый мини-погрузчик JCB. Наработка двигателя - 100 моточасов, пройдено ТО.
                                        Погрузчик работал на площадке крупной компании, выкуплен, собственник — наша
                                        компания
                                    </p>
                                    <div class="product-card__cta">
                                        <button class="btn btn_brand-yellow">ЗАПРОСИТЬ ПОДРОБНОСТИ</button>
                                    </div>
                                </div>
                                <div class="col-4 text-right">
                                    <span
                                            class="product-card__price">1 700 000 <s>Р</s></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?php require_once 'templates/documentFooter.php' ?>


<script src="dist/main.bundle.js"></script>
</body>
</html>