<?php require_once 'templates/documentHeader.php' ?>
<div class="container">
    <div class="superbanner">
        <div class="row align-items-center">
            <div class="col-3">
                <div class="superbanner__title">
                    21 ВЕК НА ДВОРЕ!
                </div>
            </div>
            <div class="col-4 text-right">
                <div class="superbanner__text">ПРОЩЕ НАПИСАТЬ СООБЩЕНИЕ, ЧЕМ РАЗБИРАТЬСЯ САМОМУ</div>
            </div>
            <div class="col-1 text-canter">
                <img src="src/gfx/arrow.png" alt="" class="superbanner__row">
            </div>
            <div class="col-4">
                <button class="btn btn_brand-yellow btn_block">Написать специалисту по запчастям</button>
            </div>
        </div>
        <div class="superbanner-headline"></div>

    </div>

</div>
<div class="container">
    <div class="content-layout">
        <div class="toolbar">
            <div class="row no-gutters">
                <div class="col-6">
                    <button class="btn btn_brand-yellow btn_hamburger" data-bind="ToggleOffCanvas"><img
                                src="src/gfx/icon-hamburger.png"
                                alt="" class="icon icon_first">
                        Показать каталог
                    </button>
                </div>
                <div class="col-6 text-right">
                    <button class="btn btn_brand-yellow btn_cart"><img src="src/gfx/icon-basket.png" alt=""
                                                                       class="icon icon_first"> Корзина
                    </button>
                </div>
            </div>
        </div>
        <div class="breadcrumbs">
            <div class="stack stack_compact">
                <div class="stack__item">КАТАЛОГ</div>
                <div class="stack__item">></div>
                <div class="stack__item"><a href="" class="link link_underline">ГИДРАВЛИКА</a></div>
            </div>
        </div>
        <h1 class="page-title">
            Гидромоторы, гидровращатели, гидронасосы
        </h1>
        <div class="catalog-listing">
            <div class="catalog-listing__thead">
                <div class="row">
                    <div class="col-2">Артикул</div>
                    <div class="col-2">Наименование</div>
                    <div class="col-2">Примечание</div>
                    <div class="col-2 text-center">Единицы</div>
                </div>
            </div>

            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <!-- -->
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <div class="catalog-listing__tr">
                <div class="row">
                    <div class="col-2">РПГ 6300</div>
                    <div class="col-2">Гидродвигатель</div>
                    <div class="col-2">Аналог МГП</div>
                    <div class="col-2 text-center">шт</div>
                    <div class="col-2 offset-2 text-right"><a href="" class="link link_underline">ЗАПРОСИТЬ</a></div>
                </div>
            </div>
            <!-- -->


        </div>
    </div>
</div>

<?php require_once 'templates/documentFooter.php' ?>


<script src="dist/main.bundle.js"></script>
</body>
</html>